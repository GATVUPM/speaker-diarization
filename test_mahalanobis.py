from scipy.stats import chi2
import numpy as np
import scipy as sp
import os

def mahalanobis(x=None, data=None, cov=None):
    """Compute the Mahalanobis Distance between each row of x and the data
    x    : vector or matrix of data with, say, p columns.
    data : ndarray of the distribution from which Mahalanobis distance of each observation of x is to be computed.
    cov  : covariance matrix (p x p) of the distribution. If None, will be computed from data.
    """
    x_minus_mu = x - np.mean(data)
    if cov is None:
        cov = np.cov(data.T)
    inv_covmat = sp.linalg.inv(cov)
    left_term = np.dot(x_minus_mu, inv_covmat)
    mahal = np.dot(left_term, x_minus_mu.T)
    return mahal.diagonal()


# ---------------------------------------------------------------------

embedding_dir = "E:\\David\\Datasets\\EasyTV\\serie\\Dataset\\audio_embeddings"
all_emb_orig_class = np.load(os.path.join(embedding_dir, "sp_emb_Claudia.npy"))
cov = np.cov(all_emb_orig_class.T)
input_shape = all_emb_orig_class.shape
centroid_orig_class = np.load(os.path.join(embedding_dir, "sp_centroid_emb_Claudia.npy")).reshape((1, input_shape[1]))

new_emb_orig_class = all_emb_orig_class[90].reshape((1, input_shape[1]))

# Not rejected hypothesis
significance_level = 0.01
critical_value = chi2.ppf((1-significance_level), df=all_emb_orig_class.shape[0]-1)

# diagonal matrix
centroid_airplane_diag = np.diag(centroid_orig_class[0])

mahalanobis_dist = mahalanobis(new_emb_orig_class, all_emb_orig_class)[0]
pvalues = 1 - chi2.cdf(mahalanobis_dist, all_emb_orig_class.shape[0]-1)

# New case
new_emb_class = np.load("models_output\\embeddings\\emb_breathing.npy")[0].reshape((1, input_shape[1]))
mahalanobis_dist_new_class = mahalanobis(new_emb_class, all_emb_orig_class)[0]
pvalues_class_air = 1 - chi2.cdf(mahalanobis_dist_new_class, all_emb_orig_class.shape[0]-1)



from sklearn.metrics import mutual_info_score

centroid_orig_class = np.load("models_output\\embeddings\\centroid_emb_frog.npy")

a = mutual_info_score(centroid_orig_class, new_emb_orig_class.reshape((input_shape[1],)), contingency=None)



from scipy.spatial.distance import pdist, squareform

# 2) Approach
centroid_emb = np.array([])
import glob

for i, file in enumerate(glob.glob("models_output\\embeddings\\*.npy")):
    if 'centroid' in file:
        new_cent = np.load(file).reshape((1, input_shape[1]))
        if i == 0:
            centroid_emb = new_cent
        else:
            centroid_emb = np.concatenate([centroid_emb, new_cent], axis=0)

all_data = np.concatenate([all_emb_orig_class, new_emb_class])
dm = pdist(all_data, "cosine")
dm2 = squareform(dm)
max = np.max(dm2)


