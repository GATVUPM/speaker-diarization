



# **Speech Analyzer Module**

This module aims to perform speaker identification, speaker recognition and speaker diarization using Python, Flask, Keras and Librosa. Most of the parameters used along this module can be modified as desired in the config.py script including the host and the port where the service is going to be deployed. 

## Voice Activity Detection
### HTTP POST Request

```bash
POST: http://0.0.0.0:5000/speech_analysis/vad
```

Header | Value
------------ | -------------
Content-Type | application/json 
Accept  | application/json

Body:
```json 
{"audioFile":  "path/to/.wav",
"save_output_locally": "True",
"plot_regions": "False",
"model": "inaVAD"
}
```
Input | Variable Type| Description | Example
------------ | ------------- | ------------- | -------------
audioFile| string | Path to wav file to be analysed |	"C:/Documents/example.wav"	
save_output_locally| boolean | Save results locally | "True"
plot_regions  | boolean | Plot the regions of the VAD module | "False"
model | string | VAD model employed in the analysis | energyVAD, inaVAD or webrtcVAD

### First Approach (Energy-based model)
Voice activity detector based on ration between energy in speech band and total energy. More details can be found at: [Voice Activity Detector]([https://github.com/marsbroshok/VAD-python](https://github.com/marsbroshok/VAD-python))

Response body:

```json 
{"Voice Activity Detection": 
		[{"event": "speech", "start": 0.79, "end": 0.88}, 
		 {"event": "speech", "start": 2.4, "end": 2.57},
		 {"event": "speech", "start": 3.35, "end": 3.57},
		 {"event": "speech", "start": 5.96, "end": 6.07},
		 {"event": "speech", "start": 7.48, "end": 7.65}],
 "Error": "False"}
```

### Second Approach (inaSpeech)
CNN-based model proposed at Doukhan, D., Carrive, J., Vallet, F., Larcher, A., & Meignier, S. (2018, April). An open-source speaker gender detection framework for monitoring gender equality. In _2018 IEEE International Conference on Acoustics, Speech and Signal Processing (ICASSP)_(pp. 5214-5218). IEEE.

Response body:

```json 
{"Voice Activity Detection": 
		[{"event": "Female", "start": 0.0, "end": 3.16},
		 {"event": "Noactivity", "start": 3.2, "end": 3.86},
		 {"event":"Female", "start": 3.86, "end": 4.48},
		 {"event": "Noactivity", "start": 4.5200000000000005, "end": 5.12},
		 {"event": "Female", "start": 5.12, "end": 10.46},
		 {"event": "Male", "start":10.46, "end": 12.16}],
 "Error": "False"}
```

### Third Approach (WebRTC )
CNN-based model proposed by Google. All the details can be found at: [WebRTC Voice Activity Detector]([https://github.com/wiseman/py-webrtcvad](https://github.com/wiseman/py-webrtcvad)).

Response body:

```json 
{"Voice Activity Detection": 
		[{"event": "speech", "start": 0.0, "end": 0.03},
		 {"event": "speech", "start": 0.03, "end": 0.06},
		 {"event": "speech", "start": 0.06, "end": 0.09},
		 {"event": "non-speech", "start": 0.09, "end": 0.12},
		 {"event": "non-speech", "start": 0.12, "end": 0.15},
		 {"event": "non-speech", "start": 0.15, "end": 0.18}, 
		 {"event": "non-speech", "start": 12.18, "end": 12.21}],
 "Error": "False"}
```

## Speaker Diarization/Recognition
A Python Library for Speaker Recognition and Speaker Diarization. The service generates Mel-spectrograms from the raw audio signal and performs a CNN model to extract potential features from it. 

Moreover, using the embeddings of such CNN model, the service yields the speakers associated to a certain audio signal.

### Dataset Folder Structure
In order to perform the analysis, the dataset must be stored using the following directory structure:
```bash
/path_to_media
	    /audio
		/character_A
		    1.wav 
			...
		/character_B
			1.wav
			...
```
### Generate a new dataset

The service access to the aforementioned directory and generates the Mel-spectrogram with the specifications written in config.py.
Moreover, it generates the following directories where the trained models, the results and the embeddings will be stored:
```bash
/path_to_media
        /audio
            /character_A
                1.wav 
                ...
            /character_B
                1.wav
                ...
        /data_output
            /audio_output
            /categories
            /models_output
                /embeddings
                /trained_models
                /trained_models_history
                /trained_models_results
```

To call this functionality, the following request is needed:

```bash
GET: http://0.0.0.0:5000/speech_analysis/speaker_diarization/generate_dataset/<media>
```
Header | Value
------------ | -------------
Content-Type | application/json 
Accept  | application/json

Response body:

```json 
{	
"Service": "Speech Analysis Service",
"Task": "Dataset Generation",
"Status": "The process ended with success!"
}
```
In case of **errors** during the process, it will be indicated in the **"Status"** field of the response.

### Train the models
To train a new CNN model based on a specific media content, the following request is needed:

```bash
GET: http://0.0.0.0:5000/speech_analysis/speaker_diarization/train/<media>
```
Header | Value
------------ | -------------
Content-Type | application/json 
Accept  | application/json

Response body:

```json 
{	
"Service": "Speech Analysis Service",
"Task": "Speaker Diarization Training",
"Status": "The process ended with success!"
}
```
In case of **errors** during the process, it will be indicated in the **"Status"** field of the response.

For this purpose, the generation of the dataset for "media" is mandatory. The model will loaded the stored spectrograms to be passed through it. The main parameters to configure the CNN model are presented in config.py.

### Generate Embeddings

The next step in the pipeline is to generate the embeddings for an specific "media" content based on the last dense hidden layer of the CNN model (from now named as **embedding layer**). To do so, the following request is employed:

```bash
GET: http://0.0.0.0:5000/speech_analysis/speaker_diarization/embedding/<media>
```
Header | Value
------------ | -------------
Content-Type | application/json 
Accept  | application/json

Response body:

```json 
{	
"Service": "Speech Analysis Service",
"Task": "Speaker Embedding Computation",
"Status": "The process ended with success!"
}
```
In case of **errors** during the process, it will be indicated in the **"Status"** field of the response.

### Predict Speakers

Finally, once the model has been trained and the embeddings for all the speakers are calculated, a new function is used to make predictions from a raw input audio file. To do so, the following request is needed:

```bash
GET: http://0.0.0.0:5000/speech_analysis/speaker_diarization/predict/<media>/<episode>
```
Header | Value
------------ | -------------
Content-Type | application/json 
Accept  | application/json

Response body:

```json 
{
"Service": "Speech Analysis Service",
"Task": "Speaker Embedding Computation",
"Status": "The process ended with success!",
"Output":[  {"speaker": "Marie", "start": 0.0, "end": 5.3},
            {"speaker": "Peter", "start": 6.03, "end": 12.23},
            {"speaker": "Peter", "start": 13.34, "end": 21.22}
         ]
}
```
In case of **errors** during the process, it will be indicated in the **"Status"** field of the response as it is showed below:

```json 
{
"Service": "Speech Analysis Service",
"Task": "Speaker Diarization Episode Analyzer",
"Status": "An error occurred when analyzing episode: " + str(episode) + ". Error information: " + str(e),
"Output": []
}
```
 Where "e" will be the error information and "episode" the episode that was analyzed. The **Output** field is measured in **seconds**.
