import os
from helper.custom_log import init_logger
from pathlib import Path

# ----------------------------------------------------
# To be changed
os.environ['media_dir'] = "E:\\David\\Datasets\\EasyTV"
#os.path.join('www', 'media')
# ----------------------------------------------------

# Web application parameters
host = "192.168.0.22"
port = "5001"

# Voice Activity Detector Parameters
output_dir = 'output'

# Webrcvad model
aggressiveness=3
window_duration=0.03
bytes_per_sample=2
sample_rate = 48000 # 8000, 16000, 32000, 48000
vad = "webrtcVAD" # energyVAD, inaVAD, webrtcVAD

# Services Name
main_service_name = "Speech Analysis Service"
vad_service_name = "Voice Activity Detection"
sp_custom_service_name = "Custom Speaker Diarization"
sp_training_service_name = "Speaker Diarization Training"
sp_predict_service_name = "Speaker Diarization Episode Analyzer"
sp_embedding_service_name = "Speaker Embedding Computation"
generate_dataset_service_name = "Dataset Generation"
error_msg = 'An error occurred when launching the service. Error: '
success_msg = "The process ended with success!"

# --------------------------------------------
# Speaker Diarization Configuration
# --------------------------------------------
if not "executables" in str(os.getcwd()):
    service_parent_dir = os.getcwd()
else:
    service_parent_dir = Path(os.getcwd()).parent

normalize_spectrogram = True # None/True/False
# None Case
if normalize_spectrogram is None:
    normalize_name = '_mfcc'
# True Case
elif normalize_spectrogram:
    normalize_name = '_normalize'
# False Case
else:
    normalize_name = '_non_normalize'

serie_dir = None
dataset_dir = "Dataset"
audio_dir = "audio"
data_output_dir = "data_output" + normalize_name
audio_output_dir = "audio_output"
categories_dir = "categories"
models_output_dir = "models_output"
embedding_dir = "embeddings"
trained_models_dir = "trained_models"
trained_models_history_dir = "trained_models_history"
trained_models_results_dir = "trained_models_results"

# DataFrame information
filename_col = "audio_path"
response = "speaker"
categories_filename = 'categories.json'

# Audio Parameters
if normalize_spectrogram is None:
    spec_type = 'mfcc'
else:
    spec_type = "mel" # or log

sr = 48000
n_mels = 2**6
n_fft = 2048
power = 2.0
fmin = 20
fmax = int(sr/2)
duration = 3 # Seconds
frame_length = int(duration*sr)
hop_length = 512

significance_level = 0.01
data_augmentation = True
streech = True
pitch = False
noise = True
rate = [1.07, 0.81]
n_steps = [2.0, 2.5]
noise_range = 3


# Training parameters
random_state=128
split_test=0.15

# Deep Learning Parameters
feature_model_name = "Conv1D" # Conv2D/Conv1D
model_name_ext = '.h5'
model_name = feature_model_name + "_speakDirNet" + model_name_ext
model_name_checkpoint = model_name.replace(model_name_ext, '_best_weights.hdf5')
model_history_name = model_name.replace(model_name_ext, "_history.json")
weight_data_name = model_name.replace(model_name_ext, "_weights.h5")
evaluation_results_name = model_name.replace(model_name_ext, "_eval_results.json")
model_plot_name = model_name.replace(model_name_ext, ".png")
embedding_name = feature_model_name + '_emb_'
centroid_emb_name = feature_model_name + '_centroid_emb_'
new_class_label = "New speaker"

metric = "accuracy"
loss = "categorical_crossentropy"
epochs = 120
k_fold = 5
batch_size = 256
embedding_layer = 'embedding'
output_act = "softmax"
hidden_act = "relu"
optimizer_name = "adam"

# Logger
logger = init_logger(__name__, testing_mode=False)