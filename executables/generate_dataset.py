import sys
sys.path.append("..")

from services.services import SpeechAnalysisService


if __name__ == '__main__':
    serv = SpeechAnalysisService()
    media = "serie_B"
    serv.init_manager()
    serv.generate_new_dataset(media)
    output = serv.output
    print(output)