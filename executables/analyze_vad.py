import sys
sys.path.append("..")

from services.services import SpeechAnalysisService

if __name__ == '__main__':
    serv = SpeechAnalysisService()
    data = {"audioFile":  "E:\\David\\Datasets\\EasyTV\\serie_A\\1\\19-198-0018.wav",
             "save_output_locally": "True",
             "plot_regions": "True",
             "model": "webrtcVAD"}
    serv.init_manager()
    serv.execute_vad(data)
    output = serv.output
