import sys
sys.path.append("..")

from services.services import SpeechAnalysisService


if __name__ == '__main__':
    serv = SpeechAnalysisService()
    media = "serie_A"
    serv.init_manager()
    serv.generate_speaker_embeddings(media)
    output = serv.output
    print(output)