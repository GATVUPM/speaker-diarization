import os
import operator
import tensorflow as tf
import numpy as np
from keras.models import Sequential, load_model, Model
from keras.layers import (Dense, Dropout, Flatten, Activation, Input,Lambda, MaxPool1D,
                          Conv1D, BatchNormalization, SpatialDropout1D, GlobalMaxPool1D,Conv2D,
                          MaxPool2D)
from keras import backend as K
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras import optimizers, regularizers
from keras.utils import to_categorical, plot_model
from helper import config as cfg
from sklearn.utils import class_weight
from helper.utilities import (write_json, generate_json_format, prepare_directory,
                              read_numpy_file, write_numpy_file, update_path_name_by_string)


class SpeakerDirNet:
    def __init__(self, input_shape, metric, loss, output_dim, output_act, hidden_act, embedding_layer,
                 optimizer_name, epochs, batch_size, parent_dir, models_output_parent_dir, embedding_dir,
                 model_trained_dir, model_eval_dir, model_trained_history_dir, model_name, feature_model_name,
                 model_history_name, weight_data_name, evaluation_results_name, embedding_filename,
                 centroid_emb_filename):

        self.input_shape = input_shape
        self.metrics = [metric]
        self.loss = loss
        self.siamese_loss = "binary_crossentropy"
        self.output_dim = output_dim
        self.output_act = output_act
        self.hidden_act = hidden_act
        self.filter_size = None
        self.kernel_size = None
        self.pool_size = None
        self.n_cnn_blocks = 3
        self.l2_loss_lambda = 0.01
        self.L1_loss_lambda = None#0.001
        self.L1 = None
        self.L2 = None
        self.embedding_layer = embedding_layer
        self.optimizer_name = optimizer_name
        self.optimizer = None
        self.padding = "same"
        self.init_model = "he_normal"
        self.feature_model_name = feature_model_name
        self.callback_elements = []
        self.epochs = epochs
        self.batch_size = batch_size
        self.model = None
        self.encoder = None
        self.siamese_model = None
        self.model_history = None
        self.parent_dir = parent_dir
        self.scores = None

        self.models_output_parent_dir = os.path.join(self.parent_dir, models_output_parent_dir)
        self.embedding_dir = os.path.join(self.models_output_parent_dir, embedding_dir)
        self.model_trained_dir = os.path.join(self.models_output_parent_dir, model_trained_dir)
        self.model_eval_dir = os.path.join(self.models_output_parent_dir, model_eval_dir)
        self.model_trained_history_dir = os.path.join(self.models_output_parent_dir, model_trained_history_dir)

        self.model_path = os.path.join(self.model_trained_dir, model_name)
        self.model_path_checkpoint = os.path.join(self.model_trained_dir, cfg.model_name_checkpoint)
        self.model_history_path = os.path.join(self.model_trained_history_dir, model_history_name)
        self.weight_path = os.path.join(self.model_trained_dir, weight_data_name)
        self.evaluation_results_path = os.path.join(self.model_eval_dir, evaluation_results_name)
        self.model_plot_name = cfg.model_plot_name

        self.embedding_name = embedding_filename
        self.centroid_emb_name = centroid_emb_filename

        self.graph = None
        self.session = None
        self.init_process()

    def init_process(self):
        try:
            prepare_directory(self.models_output_parent_dir)
            prepare_directory(self.embedding_dir)
            prepare_directory(self.model_trained_dir)
            prepare_directory(self.model_eval_dir)
            prepare_directory(self.model_trained_history_dir)
            self.extract_conv2D_params()
        except Exception as e:
            cfg.logger.error(e)
        return self

    def build_baseline_conv2D_model(self):
        try:
            self.encoder = Sequential()

            # -------------------------------- Block One
            for i in range(self.n_cnn_blocks):
                self.encoder.add(Conv2D(self.filter_size, self.kernel_size,
                                        padding=self.padding, input_shape=self.input_shape,
                                        kernel_initializer=self.init_model,
                                        kernel_regularizer=self.L1))
                self.encoder.add(Activation(self.hidden_act))
                self.encoder.add(Conv2D(self.filter_size, self.kernel_size, padding=self.padding,
                                        kernel_initializer=self.init_model,
                                        kernel_regularizer=self.L1))
                self.encoder.add(Activation(self.hidden_act))
                self.encoder.add(MaxPool2D(pool_size=self.pool_size, padding=self.padding))
                self.encoder.add(Dropout(0.35))
                # Increase filter size
                self.update_filter_kernel_size(increase_filter=True, increase_kernel=None)

            # --------------------------------  Dense One
            self.encoder.add(Flatten())
            self.encoder.add(Dense(self.filter_size, name=self.embedding_layer,
                                   kernel_regularizer=self.L1))

            self.encoder.summary()
        except Exception as e:
            cfg.logger.error(e)
        return self.encoder

    def build_baseline_conv1D_model(self, dropout=.05):
        try:
            self.encoder = Sequential()

            for i in range(self.n_cnn_blocks):
                # Add CNN Block
                self.encoder.add(Conv1D(filters=self.filter_size, kernel_size=self.kernel_size[0],
                                        padding=self.padding, activation=self.hidden_act,
                                        input_shape=self.input_shape, kernel_regularizer=self.L1))
                self.encoder.add(BatchNormalization())
                self.encoder.add(SpatialDropout1D(dropout))
                self.encoder.add(MaxPool1D())
                # Increase filter size
                self.update_filter_kernel_size(increase_filter=True, increase_kernel=None)

            # Global MaxPool
            self.encoder.add(GlobalMaxPool1D())
            # Embedding Dimension
            self.update_filter_kernel_size(increase_filter=False, increase_kernel=None)
            self.encoder.add(Dense(self.filter_size, name=self.embedding_layer))
            self.encoder.summary()

        except Exception as e:
            cfg.logger.error(e)
        return self.encoder

    def build_feature_extraction_classifier(self):
        try:
            # Select Model -----------------------------------
            if self.feature_model_name == "Conv1D":
                if len(self.input_shape) > 2:
                    # Remove Dimension
                    self.input_shape = self.input_shape[:-1]
                self.encoder = self.build_baseline_conv1D_model()
            elif self.feature_model_name == "Conv2D":
                if len(self.input_shape) < 3:
                    # Add Dimension
                    self.input_shape = self.input_shape + (1,)
            else:
                cfg.logger.warn("Not Valid model. Please go to Config.py and choose your model.")

            # --------------------------------------------------
            # Classifier ---------------------------------------
            # Input --------------------------------------------
            input_data = Input(shape=self.input_shape)
            self.encoder.name = self.embedding_layer
            # Encoding -----------------------------------------
            encoded = self.encoder(input_data)

            # Classifier layers --------------------------------
            self.update_filter_kernel_size(increase_filter=True, increase_kernel=None, step=2)
            dense = Dense(self.filter_size, kernel_initializer=self.init_model,
                           activation=self.hidden_act, kernel_regularizer=self.L1)(encoded)
            dense = Dropout(0.25)(dense)
            self.update_filter_kernel_size(increase_filter=False, increase_kernel=None)
            dense = Dense(self.filter_size, kernel_initializer=self.init_model,
                            activation=self.hidden_act, kernel_regularizer=self.L1)(dense)
            dense = Dropout(0.35)(dense)

            # Output ---------------------------------------------
            output = Dense(self.output_dim, activation=self.output_act)(dense)
            # --------------------------------------------------

            # Generate Model -------------------------------------
            self.model = Model(inputs=[input_data], outputs=output)

            # Plot Summary
            self.model.summary()
        except Exception as e:
            cfg.logger.error(e)
        return self

    def build_siamese_speakerDirNet(self):
        try:
            input_left = Input(shape=self.input_shape)
            input_right = Input(shape=self.input_shape)

            # Build model
            if self.feature_model_name == 'Conv1D':
                self.encoder = self.build_baseline_conv1D_model()
            else:
                self.encoder = self.build_baseline_conv2D_model()

            encoded_left = self.encoder(input_left)
            encoded_right = self.encoder(input_right)

            # `encoder` is any predefined network that maps a single sample
            # into an embedding space.
            # `encoder` should take an input with shape (None,) + input_shape
            # and produce an output with shape (None, embedding_dim).
            # None indicates the batch dimension.

            # Here I calculate the euclidean distance between the two encoded
            # samples though other distances can be used

            # Add a customized layer to compute the absolute difference between the encodings
            L1_layer = Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
            embedded_distance = L1_layer([encoded_left, encoded_right])

            # Add a dense+sigmoid layer here in order to use per-pair, binary
            # similar/dissimilar labels
            output = Dense(1, activation='sigmoid')(embedded_distance)
            self.siamese_model = Model(inputs=[input_left, input_right], outputs=output)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def extract_conv2D_params(self):
        try:
            # filters
            self.filter_size = 2**6
            self.pool_size = (2, 2)
            self.kernel_size = (3,3)
            self.L1 = None if self.L1_loss_lambda is None else regularizers.l1(self.L1_loss_lambda)
            self.l2 = None if self.l2_loss_lambda is None else regularizers.l2(self.l2_loss_lambda)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def update_filter_kernel_size(self, increase_filter=None, increase_kernel=None,
                                  step=1, kernel_step = (2, 2)):
        try:
            if increase_filter:
                self.filter_size = int(self.filter_size * (2**step))
            elif increase_filter is None:
                pass
            else:
                self.filter_size = int(self.filter_size / (2 ** step))

            if increase_kernel:
                self.kernel_size = tuple(map(operator.add,
                                             self.kernel_size, kernel_step))
            elif increase_kernel is None:
                pass
            else:
                self.kernel_size = tuple(map(operator.sub,
                                             self.kernel_size, kernel_step))
        except Exception as e:
            cfg.logger.error(e)
        return self

    def compile_speakerDirNet(self):
        try:
            if self.optimizer is None:
                self.select_optimizer()
            self.metrics += [self.f1_m, self.precision_m, self.recall_m]
            cfg.logger.info('Compile model ... ')
            self.model.compile(loss=self.loss, optimizer=self.optimizer, metrics=self.metrics)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def compile_siamese_speakerDirNet(self):
        try:
            if self.optimizer is None:
                self.select_optimizer()
            cfg.logger.info('Compile model ... ')
            self.siamese_model.compile(loss=self.siamese_loss, optimizer=self.optimizer,
                                       metrics=['mae'])
        except Exception as e :
            cfg.logger.error(e)
        return self

    def recall_m(self, y_true, y_pred):
        try:
            true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
            possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
            recall = true_positives / (possible_positives + K.epsilon())
        except Exception as e:
            cfg.logger.error(e)
            recall = None
        return recall

    def precision_m(self, y_true, y_pred):
        try:
            true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
            predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
            precision = true_positives / (predicted_positives + K.epsilon())
        except Exception as e:
            cfg.logger.error(e)
            precision = None
        return precision

    def cosine_distance(self, vectors):
        try:
            x,y = vectors
            x_n = K.l2_normalize(x, axis=-1)
            y_n = K.l2_normalize(y, axis=-1)
            dist = K.mean(1 - K.sum((x_n * y_n), axis=-1), axis=-1)
        except Exception as e:
            cfg.logger.error(e)
            dist = None
        return dist

    def cosine_dist_output_shape(self, shapes):
        try:
            shape1, shape2 = shapes
            #print((shape1[0], 1))
            output_shape = (shape1[0], 1)
        except Exception as e:
            cfg.logger.error(e)
            output_shape = None
        return output_shape

    def f1_m(self, y_true, y_pred):
        try:
            precision = self.precision_m(y_true, y_pred)
            recall = self.recall_m(y_true, y_pred)
        except Exception as e:
            cfg.logger.error(e)
            return None
        return 2 * ((precision * recall) / (precision + recall + K.epsilon()))

    def select_optimizer(self):
        try:
            if self.optimizer_name == 'sgd':
                self.optimizer = optimizers.SGD(learning_rate=0.01, momentum=0.0,
                                                nesterov=False)
            elif self.optimizer_name == 'rms':
                self.optimizer = optimizers.RMSprop(learning_rate=0.001, rho=0.9)
            elif self.optimizer_name == 'adagrad':
                self.optimizer = optimizers.Adagrad(learning_rate=0.01)
            elif self.optimizer_name == 'adadelta':
                self.optimizer = optimizers.Adadelta(learning_rate=1.0, rho=0.95)
            elif self.optimizer_name == 'adam':
                self.optimizer = optimizers.Adam(learning_rate=0.001, beta_1=0.9,
                                                 beta_2=0.999, amsgrad=False)
            elif self.optimizer_name == 'nadam':
                self.optimizer = optimizers.Nadam(learning_rate=0.002, beta_1=0.9,
                                                  beta_2=0.999)
            else:
                cfg.logger.warning('Not valid optimizer!. Please check the configuration script.')
        except Exception as e:
            cfg.logger.error(e)
        return self

    def train_speakerDirNet(self, x_train, y_train, x_val, y_val, unique_categories, n_classes):
        try:

            class_weights = class_weight.compute_class_weight('balanced', unique_categories,
                                                              y_train)

            # Categorical
            y_train_t = to_categorical(y_train, num_classes=n_classes)

            if y_val is not None:
                y_val_t = to_categorical(y_val, num_classes=n_classes)
                validate_data = (x_val, y_val_t)
            else:
                y_val_t = None
                validate_data = None

            earlystop = EarlyStopping(monitor='val_loss',
                                      patience=4,
                                      verbose=1,
                                      mode='min')
            self.callback_elements.append(earlystop)
            save_best_model = ModelCheckpoint(self.model_path_checkpoint, monitor='val_loss', verbose=1,
                                              save_best_only=True, save_weights_only=False,
                                              mode='min', period=1)

            self.callback_elements.append(save_best_model)
            self.model_history = self.model.fit(x_train, y_train_t, epochs=self.epochs,
                                                batch_size=self.batch_size, verbose=2,
                                                validation_data=validate_data,
                                                validation_split=0.25,
                                                class_weight=class_weights,
                                                callbacks=self.callback_elements)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def evaluate_model(self, model, x_test, y_test):
        try:
            if len(y_test.shape)<=2:
                y_test = to_categorical(y_test, num_classes=self.output_dim)
            self.scores = model.evaluate(x_test, y_test, verbose=1)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def save_history_speakerDirNet(self, k_fold=1):
        try:
            # Save model
            #model_json = self.model_history.model.to_json()
            if k_fold is not None:
                additional_str = str(k_fold)
                self.model_path = update_path_name_by_string(full_path=self.model_path, index=os.sep,
                                                             substr='_', additional_str=additional_str,
                                                             extension='.h5')
                self.weight_path = update_path_name_by_string(full_path=self.weight_path, index=os.sep,
                                                              substr='_', additional_str=additional_str,
                                                              extension='.h5')
                self.model_history_path = update_path_name_by_string(full_path=self.model_history_path,
                                                                     index=os.sep, substr='_',
                                                                     additional_str=additional_str,
                                                                     extension='.json')

            # Save Model and Weights together
            self.model.save(self.model_path)

            # Save history
            history_json = generate_json_format(input=self.model_history.history)
            write_json(json_data=history_json, json_path=self.model_history_path)
            cfg.logger.info("Saved model history at %s", self.model_history_path)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def save_evaluation_metrics(self, k_fold=1):
        output = {}
        try:
            for key, values in self.model_history.history.items():
                mean_key = key + "_mean"
                sd_key = key + "_sd"
                output[key] = {mean_key: np.mean(values), sd_key: np.std(values)}
            # Save Results
            eval_res_json = generate_json_format(output)
            if k_fold is not None:
                self.evaluation_results_path = update_path_name_by_string(full_path=self.evaluation_results_path,
                                                                          index=os.sep, substr='_',
                                                                          additional_str=str(k_fold),
                                                                          extension='.json')
            write_json(json_data=eval_res_json, json_path=self.evaluation_results_path)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def load_speakerDirNet(self, k_fold=1, best_weights=True):
        try:
            self.model_path = self.model_path.replace('.h5', '_' + str(k_fold) + '.h5')
            self.weight_path = self.weight_path.replace('.h5', '_' + str(k_fold) + '.h5')

            custom_objects={"f1_m": self.f1_m, "recall_m":self.recall_m, "precision_m":self.precision_m}

            self.model = load_model(self.model_path, custom_objects=custom_objects)

            """architecture = read_architecture(json_path=self.model_path)

            # Load model from json
            self.model = model_from_json(architecture)"""

            if best_weights:
                # load weights into new model
                self.model.load_weights(self.model_path_checkpoint)

        except Exception as e:
            cfg.logger.error(e)
        return self

    def reset_tf_session(self):
        try:
            # Reset graph
            if self.graph is not None:
                self.graph = None
            # Reset session
            if self.session is not None:
                self.session = None
            self.graph = tf.Graph()
            self.session = tf.Session(graph=self.graph)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def load_embedding_model(self, k_fold=1, best_weights=True):
        try:
            # Load Model
            self.load_speakerDirNet(k_fold=k_fold, best_weights=best_weights)
            self.model.summary()
            # Generate the embedding model
            embedding_layer_output = self.model.get_layer(self.embedding_layer).get_output_at(-1)
            self.encoder =  Model(inputs=[self.model.input], outputs=embedding_layer_output)
            self.encoder.summary()
        except Exception as e:
            cfg.logger.error(e)
        return self

    def plot_speakerDirNet(self):
        try:
            # Plot architecture
            plot_model(self.model,to_file=self.model_plot_name, show_shapes=True,
                       show_layer_names=True)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def save_embeddings_into_file(self, x_emb, x_cent_emb, cat_name):
        try:
            embedding_file = os.path.join(self.embedding_dir, self.embedding_name + str(cat_name))
            embedding_file_cent = os.path.join(self.embedding_dir, self.centroid_emb_name + str(cat_name))
            write_numpy_file(embedding_file, data=x_emb)
            write_numpy_file(embedding_file_cent, data=x_cent_emb)
        except Exception as e:
            cfg.logger.error(e)
        return self

    def load_embedding_file(self, cat_name):
        try:
            filename_emb = os.path.join(self.embedding_dir, self.embedding_name + str(cat_name) + '.npy')
            sound_embedding = read_numpy_file(numpy_filename=filename_emb)
        except Exception as e:
            cfg.logger.error(e)
            sound_embedding = None
        return sound_embedding

    def load_centroid_embedding_file(self, cat_name):
        try:
            filename_cent = os.path.join(self.embedding_dir, self.centroid_emb_name + str(cat_name) + '.npy')
            centroid_emb = read_numpy_file(numpy_filename=filename_cent)
        except Exception as e:
            cfg.logger.error(e)
            centroid_emb = None
        return centroid_emb