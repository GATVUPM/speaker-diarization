import scipy.stats
from tqdm import tqdm
import numpy as np
from helper import config as cfg
from helper.utilities import normVec


class EmbeddingManager:
    def __init__(self, speakerDirNet, categories):
        self.speakerDirNet = speakerDirNet
        self.model = None
        self.categories = categories
        self.x_emb = None
        self.centroid_emb = None

    def load_embeddings(self):
        try:
            pass
        except Exception as e:
            cfg.logger.error(e)
        return self


    def calculate_embeddings(self, x, ind):
        try:
            self.status = cfg.success_msg
            with self.speakerDirNet.session.as_default():
                with self.speakerDirNet.graph.as_default():
                    if self.model is None:
                        self.speakerDirNet.load_embedding_model(k_fold=5)
                        self.model = self.speakerDirNet.embedding_model
                    with tqdm(total=len(self.categories)) as pbar:
                        for i, cat in enumerate(self.categories):
                            pbar.update(1)
                            x_s = x[ind[cat]]
                            # Predict
                            x_emb = self.model.predict(x_s)
                            # Normalize vector
                            self.x_emb = np.array([normVec(i) for i in x_emb])
                            # Get centroid embedding
                            self.x_centroid_emb = np.mean(x_emb, axis=0)
        except Exception as e:
            cfg.logger.error(e)
            self.status = cfg.error_msg + str(e)
        return self
