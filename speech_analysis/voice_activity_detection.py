import json
import soundfile as sf
import librosa
from VAD import vad
from helper import utilities
from pydub import AudioSegment
from pydub.utils import mediainfo
import os
import webrtcvad
import struct
import numpy as np
import matplotlib.pyplot as plt
from scipy.io import wavfile
from helper import config as cfg
from inaSpeechSegmenter import Segmenter
import sys
if not sys.warnoptions:
    import warnings
    warnings.simplefilter("ignore")


class VoiceActivityDetection:
    def __init__(self, parent_dir, audiofile, save, outputFile, plot_regions=True):
        self.parent_dir = parent_dir
        self.audiofile = audiofile
        self.raw_audio = None
        self.save = save
        self.samples = None
        self.sample_rate = None
        self.energy_vad = None
        self.inaVad = None
        self.webrtcvad = None
        self.segment_labels = None
        self.aggressiveness = cfg.aggressiveness
        self.window_duration = cfg.window_duration
        self.bytes_per_sample = cfg.bytes_per_sample
        self.output_dir = cfg.output_dir
        self.re_sample_rate = cfg.sample_rate
        self.output = {cfg.vad_service_name: [], 'Error': "False"}
        if outputFile is not None:
            self.outputFile = os.path.join(self.parent_dir, self.output_dir, outputFile)
        else:
            self.outputFile = None
        self.plot_regions = plot_regions
        self._convert_to_wav()

    def _convert_to_wav(self):
        try:
            wav_format = '.wav'
            if self.audiofile.split('.')[-1] == 'mp3':
                self.raw_audio = AudioSegment.from_mp3(self.audiofile)
                self.audiofile = self.audiofile.split('.')[0] + wav_format
                self.raw_audio.export(self.audiofile.replace('mp3', 'wav'), format="wav")
            elif self.audiofile.split('.')[-1] == 'ogg':
                sound = AudioSegment.from_ogg(self.audiofile)
                self.audiofile = self.audiofile.split('.')[0] + wav_format
                sound.export(self.audiofile.replace('ogg', 'wav'), format="wav")
            elif self.audiofile.split('.')[-1] == 'wav':
                pass
            else:
                cfg.logger.warning('Audio file is not neither .mp3 nor wav nor ogg')
                return None
        except Exception as e:
            cfg.logger.error(e)
            self.output['Error'] = str(e)
        return self

    def _get_media_info(self):
        try:
            # 1) Access to Media info
            info = mediainfo(self.audiofile)

            # Convert to Mono
            num_channels = int(info['channels'])
            sample_rate = int(info['sample_rate'])

            if num_channels != 1:
                sound = AudioSegment.from_wav(self.audiofile)
                sound = sound.set_channels(1)
                sound.export(self.audiofile, format="wav")
        except Exception as e:
            cfg.logger.error(e)
            self.output['Error'] = str(e)
            num_channels = None
            sample_rate = None
        return num_channels, sample_rate

    def _apply_resampling(self, sample_rate):
        try:
            if sample_rate not in [8000, 16000, 32000, 48000]:
                ori_samples, sample_rate = sf.read(self.audiofile)
                re_samples = librosa.resample(ori_samples, sample_rate, self.re_sample_rate )
                sf.write(self.audiofile, re_samples, self.re_sample_rate)
        except Exception as e:
            cfg.logger.error(e)
            self.output['Error'] = str(e)
        return self

    def _prepare_output(self, model):
        try:
            start_key = 'start'
            end_key = 'end'
            event_key = 'event'
            cfg.logger.info('Preparing output ...')
            old_data = {}

            for out in self.segment_labels:
                # inaSpeech model
                if model == 'inaVAD':
                    # Activity detected
                    act = out[0].title()
                    start = out[1]
                    end = out[2]
                    old_data = {event_key: act, start_key: start, end_key: end}

                # vad model
                elif model=='energyVAD':
                    old_data = {event_key: 'speech', start_key: out['speech_begin'], end_key: out['speech_end']}

                elif model=='webrtcVAD':
                    if out['is_speech']:
                        act = 'speech'
                    else:
                        act = 'non-speech'
                    old_data = {event_key: act, start_key: out['start'], end_key: out['stop']}
                else:
                    pass
                # Add data
                self.output[cfg.vad_service_name].append(old_data)

        except Exception as e:
            cfg.logger.error(e)
            self.output['Error'] = str(e)
        return self

    def _run_inaVAD(self):
        try:
            # create an instance of speech segmenter
            # this loads neural networks and may last few seconds
            # Warnings have no incidence on the results
            cfg.logger.info('Detecting speech activity in audiofile %s using inaVAD', self.audiofile)

            if self.inaVad is None:
                self._load_segmenter_model()

            # segmentation is performed using the __call__ method of the segmenter instance
            self.segment_labels = self.inaVad(self.audiofile)

            self._prepare_output(model='inaVAD')

            if self.save:
                self._save_to_file(data=self.output)

        except Exception as e:
            cfg.logger.error(e)
            self.output['Error'] = str(e)
        return self

    def _run_energyVAD(self):
        try:
            cfg.logger.info('Detecting speech activity in audiofile %s using Energy VAD', self.audiofile)
            self.energy_vad = vad.VoiceActivityDetector(self.audiofile)
            raw_detection = self.energy_vad.detect_speech()

            if self.plot_regions:
                self.energy_vad.plot_detected_speech_regions(raw_detection)

            self.segment_labels = self.energy_vad.convert_windows_to_readible_labels(raw_detection)

            # Normalize the output
            self._prepare_output(model='energyVAD')
            if self.save:
                self._save_to_file(data=self.output)

        except Exception as e:
            cfg.logger.error(e)
            self.output['Error'] = str(e)
        return self

    def _run_webrtcVAD(self):
        try:
            cfg.logger.info('Detecting speech activity in audiofile %s using Webrtc VAD', self.audiofile)

            # Audio preprocessing
            num_channels, self.sample_rate = self._get_media_info()
            # Check valid sample rate
            self._apply_resampling(sample_rate=self.sample_rate)

            # Load data
            self.sample_rate, self.samples = wavfile.read(self.audiofile)
            self.webrtcvad = webrtcvad.Vad()

            # set aggressiveness from 0 to 3
            self.webrtcvad.set_mode(self.aggressiveness)

            raw_samples = struct.pack("%dh" % len(self.samples), *self.samples)
            samples_per_window = int(self.window_duration * self.sample_rate + 0.5)

            self.segment_labels = []

            for start in np.arange(0, len(self.samples), samples_per_window):
                stop = min(start + samples_per_window, len(self.samples))
                try:
                    is_speech = self.webrtcvad.is_speech(raw_samples[start * self.bytes_per_sample:
                                                                     stop * self.bytes_per_sample],
                                                         sample_rate=self.sample_rate)

                    # Convert samples to time (s)
                    start_s = start / self.sample_rate
                    stop_s = stop / self.sample_rate

                    self.segment_labels.append(dict(start=start_s,stop=stop_s,is_speech=is_speech))
                except Exception as e:
                    cfg.logger.warning(e)
                    continue
            self._plot_regions()
            # Normalize the output
            self._prepare_output(model='webrtcVAD')
            if self.save:
                self._save_to_file(data=self.output)

        except Exception as e:
            cfg.logger.error(e)
            self.output['Error'] = str(e)
        return self

    def _save_to_file(self, data):
        try:
            cfg.logger.info('Saving Voice Activity Results at %s ', self.outputFile)
            utilities.prepare_directory(dir_path=self.output_dir)

            with open(self.outputFile, 'w') as fp:
                json.dump(data, fp)

        except Exception as e:
            cfg.logger.error(e)
            self.output['Error'] = str(e)
        return self

    def _load_segmenter_model(self):
        try:
            self.inaVad = Segmenter()
            cfg.logger.info('Voice Activity Detector Model loaded with success!')
        except Exception as e:
            msg = 'It was not possible to load the Voice Activity Detector Model. Error: ' + str(e)
            cfg.logger.error(msg)
        return self

    def _plot_regions(self):
        try:
            name = 'vad_' + str(self.audiofile.split(os.sep)[-1].split('.')[0]) + '.png'
            filepath = os.path.join(self.parent_dir, 'Figures', name)
            if self.webrtcvad is not None:
                ymax = max(self.samples)
                ymin = min(self.samples)
                y_v = np.linspace(ymin * 1.1, ymax * 1.1, 10000)
                time = np.linspace(0, len(self.samples) / self.sample_rate,
                                   num=len(self.samples))

                plt.figure(figsize=(10, 7))
                plt.plot(time, self.samples)
                for i, segment in enumerate(self.segment_labels):
                    if segment['is_speech']:
                        x_v_1 = segment['start'] * np.ones(len(y_v))
                        x_v_2 = segment['stop'] * np.ones(len(y_v))
                        plt.plot([segment['start'], segment['stop'] - 1], [ymax * 1.1, ymax * 1.1], color='orange')
                        plt.plot([segment['start'], segment['stop'] - 1], [ymin * 1.1, ymin * 1.1], color='orange')
                        plt.plot(x_v_1, y_v, color='green', alpha=0.15)
                        plt.plot(x_v_2, y_v, color='green', alpha=0.15)
                plt.xlabel('Time (s)')
                plt.ylabel('Amplitude')
                plt.grid()
                plt.savefig(filepath)
                plt.show()
                print(filepath)
        except Exception as e:
            cfg.logger.error(e)
        return self


