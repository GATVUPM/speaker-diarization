from helper import config as cfg
import numpy as np
import librosa
import speechpy
import librosa.display
import matplotlib.pyplot as plt
import os
import h5py
from helper.utilities import get_nearest_even_number
from sklearn.preprocessing import scale

class AudioPreprocessing:
    def __init__(self, spec_type='mel', sr=44100, n_mels=96, n_fft=2048, hop_length=512,
                 power=2.0, fmax=14000, fmin=20, frame_length=1024, response=None, filename_col=None,
                 data_augmentation=False, apply_strech=True, apply_pitch_mod=True,
                 apply_noise=True, audio_output_dir=None):

        self.spec_type = spec_type
        self.sr = sr
        self.n_mels = n_mels
        self.n_fft = n_fft
        self.hop_length = hop_length
        self.power = power
        self.fmax = fmax
        self.fmin = fmin
        self.frame_length = get_nearest_even_number(frame_length)
        self.apply_strech = apply_strech
        self.apply_pitch_mod = apply_pitch_mod
        self.apply_noise = apply_noise
        self.target_col = response
        self.filename_col = filename_col
        self.data_augmentation = data_augmentation
        self.rate_strech = cfg.rate
        self.n_steps_pitch = cfg.n_steps
        self.noise_range = cfg.noise_range
        self.audio_output_dir = audio_output_dir
        self.input_length = None

    def add_noise(self, data):
        noise = np.random.randn(len(data))
        data_noise = data + 0.005 * noise
        return data_noise

    def shift(self, data):
        return np.roll(data, 1600)

    def pitch_shift(self, data, n_steps):
        try:
            input_length = len(data)
            data = librosa.effects.pitch_shift(data, sr=input_length, n_steps=n_steps)
            if len(data) > input_length:
                data = data[:input_length]
            else:
                data = np.pad(data, (0, max(0, input_length - len(data))), "constant")
        except Exception as e:
            cfg.logger.error(e)
        return data

    def stretch(self, data, rate=1.0):
        input_length = len(data)
        data = librosa.effects.time_stretch(data, rate)
        if len(data) > input_length:
            data = data[:input_length]
        else:
            data = np.pad(data, (0, max(0, input_length - len(data))), "constant")
        return data

    def load_audio_file(self, file_path):
        data = None
        try:
            data = librosa.load(file_path, sr=self.sr)[0]
        except Exception as e:
            cfg.logger.error(e)
        return data

    def trim_audio_signal(self, x):
        try:
            frame_length = int(self.hop_length*4)
            yt, index = librosa.effects.trim(x, top_db=30, hop_length=self.hop_length,
                                             frame_length=frame_length)
            #yt = np.trim_zeros(x)
        except Exception as e:
            cfg.logger.error(e)
            yt = x
        return yt

    def analyze_audio_frame(self, s, offset):
        done = True
        try:
            s_init = offset
            s_end = int(s_init + self.frame_length)
            if s_end > len(s):
                s_end = -1
                done = False
            # Frame
            frame = s[s_init:s_end]

        except Exception as e:
            cfg.logger.error(e)
            done = False
            frame = None
        return done, frame

    def extract_features(self, x, normalize=False, plot=False):
        try:
            # Check if MFCC or spectrogram
            if self.spec_type == 'mfcc':
                y_axis = None
                title = 'MFCC'
                mfcc_feature = librosa.feature.mfcc(y=x, sr=self.sr, n_mfcc=self.n_mels,
                                                    n_fft=self.n_fft, hop_length=self.hop_length,
                                                    fmin=self.fmin, fmax=self.fmax)
                mfcc_delta_feaature = librosa.feature.delta(mfcc_feature)
                # Normalize
                mfcc_cmvnw_feature = speechpy.processing.cmvnw(mfcc_feature, win_size=301, variance_normalization=True)

                # Concatenate
                feature = np.vstack((mfcc_cmvnw_feature, mfcc_delta_feaature))
                db_spect = mfcc_feature
            else:
                # Mel-spectrogram
                if self.spec_type == 'mel':
                    y_axis = 'mel'
                    title = 'Mel spectrogram'
                    power_spect = librosa.feature.melspectrogram(y=x, sr=self.sr, n_mels=self.n_mels,
                                                                 n_fft=self.n_fft, hop_length=self.hop_length,
                                                                 fmin=self.fmin, fmax=self.fmax, power=self.power)

                # Log-Spectrogram
                else:
                    y_axis = 'log'
                    title = 'Log-Power spectrogram'
                    s_fft = librosa.stft(x, n_fft=self.n_fft, hop_length=self.hop_length)
                    power_spect = np.abs(s_fft) ** self.power  # Power spectrogram

                # Convert power-spectrogram to db-spectrogram
                db_spect = librosa.power_to_db(power_spect, ref=np.max)

                # Normalize spectrogram
                if normalize:
                    spect_log = np.log(power_spect + 1e-9)
                    feature = librosa.util.normalize(spect_log)
                else:
                    feature = db_spect
            # Plot
            if plot:
                plt.figure()
                librosa.display.specshow(db_spect, sr=self.sr, y_axis=y_axis, x_axis='time')
                plt.colorbar(format='%+2.0f dB')
                plt.title(title)
                plt.tight_layout()
                plt.show()

            # Add new dimension
            feature = feature[np.newaxis, :]
        except Exception as e:
            cfg.logger.error(e)
            feature = None
        return feature

    def apply_data_augmentation(self, frames, normalize=False):
        try:
            augmented_s = []
            if self.apply_strech:
                for i, r in enumerate(self.rate_strech):
                    y_changed = self.stretch(data=frames, rate=r)
                    y_changed_s = self.extract_features(y_changed, normalize=normalize)
                    if y_changed_s is not None:
                        augmented_s.append(y_changed_s)
            if self.apply_pitch_mod:
                for j, st in enumerate(self.n_steps_pitch):
                    y_changed = librosa.effects.pitch_shift(frames, self.sr, n_steps=st)
                    y_changed_s = self.extract_features(y_changed, normalize=normalize)
                    if y_changed_s is not None:
                        augmented_s.append(y_changed_s)
            if self.apply_noise:
                # noise
                for i, l in enumerate(range(self.noise_range)):
                    y_changed = self.add_noise(data=frames)
                    y_changed_s = self.extract_features(y_changed, normalize=normalize)
                    if y_changed_s is not None:
                        augmented_s.append(y_changed_s)
        except Exception as e:
            cfg.logger.error(e)
            augmented_s = []
        return augmented_s

    def save_data_locally(self, data, mode):
        try:
            x, y = zip(*data)
            x_h = np.stack(np.array(x), axis=0)
            x = np.squeeze(x_h, axis=1)
            y = np.stack(np.array(y), axis=0)
            y = y.reshape((y.shape[0], 1))

            # Generate paths
            name_x = 'x_' + str(mode) + '.h5'
            name_y = 'y_' + str(mode) + '.h5'
            x_path = os.path.join(self.audio_output_dir, name_x)
            y_path = os.path.join(self.audio_output_dir, name_y)

            # if the file already exists, update data
            if os.path.exists(x_path) and os.path.exists(y_path):
                with h5py.File(x_path, 'a') as hf:
                    hf[name_x.replace('.h5', '')].resize((hf[name_x.replace('.h5', '')].shape[0] + x.shape[0]), axis=0)
                    hf[name_x.replace('.h5', '')][-x.shape[0]:] = x
                with h5py.File(y_path, 'a') as hf:
                    hf[name_y.replace('.h5', '')].resize((hf[name_y.replace('.h5', '')].shape[0] + y.shape[0]), axis=0)
                    hf[name_y.replace('.h5', '')][-y.shape[0]:] = y

            # New File
            else:
                max_shape = (None, x.shape[1], x.shape[2])
                with h5py.File(x_path, 'w') as hf:
                    hf.create_dataset(name_x.replace('.h5', ''), data=x, maxshape=max_shape, compression="gzip",
                                      compression_opts=4, chunks=True)

                max_shape_y = (None, y.shape[1])
                with h5py.File(y_path, 'w') as hf:
                    hf.create_dataset(name_y.replace('.h5', ''), data=y, maxshape=max_shape_y)

        except Exception as e:
            cfg.logger.error(e)
        return self

    def normalize_audio_segment(self, frame, frame_length):
        try:
            normalize_frame = frame
            if len(frame) < frame_length:
                max_offset = int(frame_length - len(frame))
                if max_offset > 0:
                    offset = np.random.randint(max_offset)
                    normalize_frame = np.pad(frame, (offset, frame_length - len(frame) - offset),
                                             "constant")
            else:
                max_offset = int(len(frame) - frame_length)
                if max_offset > 0:
                    offset = np.random.randint(max_offset)
                    normalize_frame = frame[offset:(frame_length + offset)]
        except Exception as e:
            cfg.logger.error(e)
            normalize_frame = None
        return normalize_frame

    def load_dataset(self, mode):
        d = None
        try:
            name_x = 'x_' + str(mode) + '.h5'
            name_y = 'y_' + str(mode) + '.h5'
            x_path = os.path.join(self.audio_output_dir, name_x)
            y_path = os.path.join(self.audio_output_dir, name_y)

            cfg.logger.info('Loading locally x_%s', str(mode))

            with h5py.File(x_path, 'r') as hf:
                x = hf[name_x.replace('.h5', '')][:]

            cfg.logger.info('Loading locally y_%s', str(mode))
            with h5py.File(y_path, 'r') as hf:
                y = hf[name_y.replace('.h5', '')][:]
            d = (x, y)
        except Exception as e:
            cfg.logger.error(e)
        return d

